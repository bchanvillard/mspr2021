package com.example.gostyleapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_list_code.*

class ListCodeActivity : AppCompatActivity(), (QRCodeModel) -> Unit {
    private val qrCodeRepo: QRCodeRepo = QRCodeRepo()
    private var dataList: ArrayList<QRCodeModel> = ArrayList()
    private val qrCodeAdapter: DataListAdapter = DataListAdapter(dataList, this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_code)

        val qrcodeId = intent.getStringExtra("qrCodeId")!!
        dataList.add(qrCodeRepo.GetQRCodeById(qrcodeId))

        qrCodeAdapter.dataList = dataList
        qrCodeAdapter.notifyDataSetChanged()

        listViewCodePromo.layoutManager = LinearLayoutManager(this)
        listViewCodePromo.adapter = qrCodeAdapter

        btn_return_details.setOnClickListener {
            startActivity(Intent(this@ListCodeActivity, MainActivity::class.java))
            finish()
        }
    }

    override fun invoke(model: QRCodeModel) {

    }
}