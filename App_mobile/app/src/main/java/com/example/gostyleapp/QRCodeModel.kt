package com.example.gostyleapp

import com.google.gson.annotations.SerializedName

data class QRCodeModel(
    @SerializedName("qr-code-id") val QRCodeID: String,
    @SerializedName("valeur-code-promo") var ValPromo: Float,
    @SerializedName("date-debut-promotion") val StartDatePromo: String,
    @SerializedName("date-fin-promotion") val FinishDatePromo: String,
    @SerializedName("categorie") val Category: String,
    @SerializedName("description") val Description: String,
    @SerializedName("statut") var Statut: Boolean
)