package com.example.gostyleapp

import com.google.gson.Gson
import java.net.URL

class QRCodeRepo {
    fun GetAllQRCode(){
        val url = URL("http://localhost:8080/ords/testuser/get/qrcode/all")

    }

    fun PostAllQRCode(){
        val url = URL("http://localhost:8080/ords/testuser/")
    }

    fun GetQRCodeById(id: String): QRCodeModel {
        //val url = URL("http://localhost:8080/ords/testuser/get/qr-code-id/$id").readText()
        //Test en dur pour le moment
        val data = ("""
        {
           "items":[
              {
                 "qr-code-id":"12ER41DD41",
                 "valeur-code-promo":20,
                 "date-debut-promotion":"2021-03-24T14:12:44Z",
                 "date-fin-promotion":"2021-04-09T10:12:47Z",
                 "categorie":"PANTALON",
                 "description":"Ceci est une description",
                 "statut":true
              }
           ],
           "hasMore":false,
           "limit":25,
           "offset":0,
           "count":1,
           "links":[
              {
                 "rel":"self",
                 "href":"http://localhost:8080/ords/testuser/get/qr-code-id/12ER41DD41"
              },
              {
                 "rel":"describedby",
                 "href":"http://localhost:8080/ords/testuser/metadata-catalog/get/qr-code-id/item"
              },
              {
                 "rel":"first",
                 "href":"http://localhost:8080/ords/testuser/get/qr-code-id/12ER41DD41"
              }
           ]
        }
        """)
        val result = data.substringAfter("\"items\":[").substringBefore("],")
        println(result)

        val gson = Gson()
        val qrcode: QRCodeModel = gson.fromJson(result, QRCodeModel::class.java)
        println("> From JSON String:\n$qrcode")

        return qrcode
    }
}

